let express = require('express');
let express_graphql = require('express-graphql');
let {buildSchema} = require('graphql');

//GraphQl Schema

let schema = buildSchema(`
    type Query {
        message: String
    }
`);

//root resolver
let root = {
    message: ()=> 'Hello World!'
};

//create an express server and a graphql endpoint

let app = express();
app.use('/graphql', express_graphql({
    schema: schema,
    rootValue: root,
    graphiql: true
}));
app.listen(4000, () => console.log('Express GraphQL Server Now Running On loclahost:4000/graphql'));

